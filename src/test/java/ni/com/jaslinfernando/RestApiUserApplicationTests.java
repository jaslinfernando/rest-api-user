package ni.com.jaslinfernando;

import com.fasterxml.jackson.databind.ObjectMapper;
import ni.com.jaslinfernando.dto.PhoneDto;
import ni.com.jaslinfernando.dto.UserDto;
import ni.com.jaslinfernando.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
class RestApiUserApplicationTests {

	private static final ObjectMapper om = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	UserRepository userRepository;

	@Test
	@WithMockUser(username = "lieserl", password = "Fernanda21@")
	public void testAuthenticacion() throws Exception {
		Map<String, Object> hash = new HashMap<>();
		hash.put("username", "lieserl");
		hash.put("password", "Fernanda21@");
		Map<String, Object> token = om.readValue(mockMvc.perform(post("/authenticar")
				.contentType(MediaType.APPLICATION_JSON)
				.content(om.writeValueAsString(hash)))
 				.andDo(print())
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), HashMap.class);
		assertThat(token.get("jwttoken")).isNotEqualTo(null);
	}

	@Test
	@WithMockUser(username = "lieserl", password = "LieserFernanda21@")
	public void testGuardarUsuario() throws Exception {
		UserDto userDto = new UserDto();
		userDto.setName("Lieserl Test");
		userDto.setEmail("lieserlfernanda@gmail.com");
		userDto.setPassword("LieserFernanda21@");

		PhoneDto phone = new PhoneDto();
		phone.setNumber("50582621479");
		phone.setCityCode("MGA");
		phone.setCountryCode("NIC");

		userDto.setPhones(new HashSet<PhoneDto>() {{
			add(phone);
		}});

		assertThat(om.readValue(mockMvc.perform(post("/api/users/guardar")
				.contentType(MediaType.APPLICATION_JSON)
				.content(om.writeValueAsString(userDto)))
				.andDo(print())
				.andExpect(status().isCreated())
				.andReturn().getResponse().getContentAsString(), Object.class)).isNotEqualTo(null);
	}



}
