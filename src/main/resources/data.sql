--user
INSERT INTO users (id, name, email, password, last_login, token, is_active,created, modified) VALUES
('ba4a440f-0451-42ea-8940-1432aaef831f',  'Geli Flores', 'gelimaria@gmail.com', '$2a$12$YdGDZrZI3SJxWKxh7BMGRuvyBoquMqIIt7uV0KKfvLDSWrGLfu04.', '2023-12-07T09:15:07.123456789', 'eyJzdWIiOiJqYXNsaW5mZXJuYW5kb0BnbWFpbC5jb20iLCJleHAiOjE2NzcyNjk1NDgsImlhdCI6MTY3NzI2Nzc0OH0.aDezDzKZ6A2e6rJQY-s992iUJ_ILCeuI6fEYyyQb6uADPbLAjFRWP7a4SP9uk_Oprsr0zONEAl4TLJBlnDx8Kg', true,'2023-02-06T02:27:07', null);

--
INSERT INTO phones (user_id, number, city_code, country_code,created, modified) VALUES
('ba4a440f-0451-42ea-8940-1432aaef831f', '50582621479', 'MGA', 'NCA','2023-02-11T06:30:03', null);
/**/
