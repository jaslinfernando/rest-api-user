drop table if exists users;
create table users (
 id uuid primary key,
 name varchar(20) not null,
 email varchar(200) not null,
 password varchar(200) not null,
 created timestamp not null,
 modified timestamp,
 last_login timestamp not null,
 token varchar(500),
 is_active boolean not null

);

drop table if exists phones;
create table phones(
 id int auto_increment primary key,
 user_id uuid not null,
 number varchar(20) not null,
 city_code varchar(3) not null,
 country_code varchar(3) not null,
 created timestamp not null,
 modified timestamp,
 foreign key (user_id) references users(id)
)