package ni.com.jaslinfernando.repository;

import ni.com.jaslinfernando.model.entity.EUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see
 * @since 1.0,
 */
@Repository
public interface UserRepository extends JpaRepository<EUser, UUID> {

    //puede que devuelva o no un registro
    //Optional.empty
    //Optional[EUser(id=bf0f20c2-009a-4d32-bf16-bc76836a3965, name=Jaslin Flores, email=jaslinfernando@gmail.com, password=$2a$10$OLo/wEr7nxqpZc.8p4UFfOGGvvysUvQwuADIiKQt506XoLp5m00Mi, token=eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqYXNsaW5mZXJuYW5kb0BnbWFpbC5jb20iLCJleHAiOjE2NzczNTI5NzIsImlhdCI6MTY3NzM1MTE3Mn0.PQYwJ8GCv_6MTaS-ax3hgPsV6gfnamIre3twkV1knQvdpXtzLsm9RV0OclhhJTksd1xaWNxnR9j3bJurjKZeDA, active=true, lastLogin=2023-02-25T12:52:52.375, created=2023-02-25T12:52:52.375, modified=null)]
    Optional<EUser> findUserEntityByEmail(String email);
    EUser findUserByName(String name);
    EUser findEUserById(UUID id);

}
