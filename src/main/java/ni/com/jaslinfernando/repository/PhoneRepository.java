package ni.com.jaslinfernando.repository;

import ni.com.jaslinfernando.model.entity.EPhone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<EPhone, Integer> {

}
