package ni.com.jaslinfernando.exception;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see java.lang.Exception
 * @since 1.0,
 */
public class CustomException extends java.lang.Exception {

    public CustomException() {
    }

    public CustomException(final String message) {
        super(message);
    }


    public CustomException(final String message, final Throwable cause) {
        super(message, cause);
    }


    public CustomException(final Throwable cause) {
        super(cause);
    }

    public CustomException(final String message, final Throwable cause, final boolean enableSuppression,
                           final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
