package ni.com.jaslinfernando.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see EqualsAndHashCode
 * @see ToString
 * @see Entity
 * @see Table
 * @see EUser
 * @see Serializable
 * @since 1.0, <i> Entidad de Telefonos.</i>
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "phones")
public class EPhone implements Serializable {

    private static final long serialVersionUID = 1L;

    public EPhone() {
    }

    public EPhone(final Integer id, final EUser user, final String number, final String cityCode,
                  final String countryCode) {
        this.id = id;
        this.user = user;
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
    }

    public EPhone(final Integer id, final EUser user, final String number, final String cityCode,
                  final String countryCode, final LocalDateTime created, final LocalDateTime modified) {
        this.id = id;
        this.user = user;
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private EUser user;

    @Column(name = "number")
    private String number;


    @Column(name = "city_code")
    private String cityCode;


    @Column(name = "country_code")
    private String countryCode;

    @CreatedDate
    @Column(name = "created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "modified")
    private LocalDateTime modified;

}
