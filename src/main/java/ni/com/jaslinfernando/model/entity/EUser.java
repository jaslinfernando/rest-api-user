package ni.com.jaslinfernando.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see Getter
 * @since 1.0
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "users")
public class EUser implements Serializable {

    private static final long serialVersionUID = 1L;

    public EUser() {

    }


    public EUser(final UUID id, final String name, final String email, final String password,
                 final String token, final Boolean active, final Set<EPhone> phones,
                 final LocalDateTime lastLogin, final LocalDateTime created, final LocalDateTime modified) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.token = token;
        this.active = active;
        this.phones = phones;
        this.lastLogin = lastLogin;
    }


    @Id
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @GeneratedValue(generator = "uuid")
    @Column(name = "id")
    private UUID id;

    @NotEmpty(message = "Nombre Requerido.")
    @Column(name = "name")
    private String name;


    @Email(regexp = "^[^@]+@[^@]+\\.[a-zA-Z]{2,}$", message = "Formato de correo invalido.")
    @NotNull(message = "Correo Requerido.")
    @Column(name = "email")
    private String email;

    @NotEmpty(message = "Clave Requerida.")
    @Column(name = "password")
    private String password;

    @Column(name = "token")
    private String token;

    @Column(name = "is_active")
    private Boolean active;


    @Column(name = "last_login")
    private LocalDateTime lastLogin;

    @CreatedDate
    @Column(name = "created")
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "modified")
    private LocalDateTime modified;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    @LazyCollection(LazyCollectionOption.FALSE)
    //coleccion que contiene objetos sin duplicar
    private Set<EPhone> phones = new HashSet<>();


}
