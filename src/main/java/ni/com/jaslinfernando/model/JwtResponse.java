package ni.com.jaslinfernando.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;


/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see Getter
 * @see AllArgsConstructor
 * @since 1.0,
 */
@Getter
@AllArgsConstructor
public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;

    private final String jwttoken;

}
