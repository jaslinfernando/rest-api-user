package ni.com.jaslinfernando.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see Getter
 * @since 1.0,
 */
@Getter
@Setter
@AllArgsConstructor
public class JwtRequest implements Serializable {

    private static final long serialVersionUID = 5926468583005150707L;

    @NotNull(message = "Usuario Requerido.")
    private String username;


    @NotNull(message = "Clave Requerida")
    private String password;


    public JwtRequest() {

    }
}
