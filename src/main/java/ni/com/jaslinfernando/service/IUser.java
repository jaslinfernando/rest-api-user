package ni.com.jaslinfernando.service;

import ni.com.jaslinfernando.dto.UserDto;
import ni.com.jaslinfernando.exception.CustomException;
import ni.com.jaslinfernando.model.entity.EUser;

import java.util.List;
import java.util.UUID;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @since 1.0,
 * @see
 */
public interface IUser {

    List<EUser> findAll();
    EUser findUserByName(String name)  throws CustomException;
    EUser findEUserById(UUID id) throws CustomException;
    EUser saveUser(UserDto user) throws CustomException;

}
