package ni.com.jaslinfernando.service;

import ni.com.jaslinfernando.model.entity.EUser;
import ni.com.jaslinfernando.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see UserDetailsService
 * @since 1.0, <i>JWTUserDetailsService implementa la interfaz Spring Security UserDetailsService. Reemplaza el
 * loadUserByUsername para obtener detalles de usuario de la base de datos utilizando el nombre de usuario. Spring
 * Security Authentication Manager llama a este método para obtener los detalles del usuario de la base de datos al
 * autenticar los detalles del usuario proporcionados por el usuario.</i>
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public JwtUserDetailsService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        if ("lieserl".equals(username)) { //clave: Fernanda21@
            return new User("lieserl", "$2a$12$AVieG/gx6f7fOu8jZG1ut.DRqXRAoQxanmawDlT7c95C8LJ1JEV8m",new ArrayList<>());
        }
        Optional<EUser> userEntity = userRepository.findUserEntityByEmail(username);
        if (userEntity.isPresent()) {
            userEntity.get().setLastLogin(LocalDateTime.now());
            if (!userEntity.get().getActive()) {
                throw new DisabledException("Usuario ".concat(username).concat("  inactivo."));
            }
            userRepository.save(userEntity.get());
            return new User(userEntity.get().getEmail(), userEntity.get().getPassword(), new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("No se encontro usuario: ".concat(username));
        }
    }
}
