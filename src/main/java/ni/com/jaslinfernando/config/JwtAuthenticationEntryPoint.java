package ni.com.jaslinfernando.config;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see AuthenticationEntryPoint
 * @see HttpServletRequest
 * @see AuthenticationException
 * @see HttpServletResponse
 * @since 1.0, <i>Esta clase extenderá la clase AuthenticationEntryPoint de Spring y reemplazará su método begin.
 * Rechaza cada solicitud no autenticada y envía el código de error 401.</i>
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    /**
     * variable de serializacion.
     */
    private static final long serialVersionUID = -7858869558953243875L;

    /**
     * Sobreescritura de metodo.
     * @param request solicitud.
     * @param response response.
     * @param authException exception.
     * @throws IOException checked exception.
     */
    @Override
    public void commence(final HttpServletRequest request, final HttpServletResponse response,
                         final AuthenticationException authException) throws IOException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
    }
}
