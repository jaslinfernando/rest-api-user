package ni.com.jaslinfernando.config;

import io.jsonwebtoken.ExpiredJwtException;
import ni.com.jaslinfernando.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/***
 * JwtRequestFilter extiende la clase Spring Web Filter OncePerRequestFilter.
 * Para cualquier solicitud entrante,
 * se ejecuta esta clase Filter. Comprueba si la solicitud tiene un token JWT válido.
 * Si tiene un token JWT válido, establece la autenticación en el contexto,
 * para especificar que el usuario actual está autenticado.
 */
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    /**
     * inyeccion de dependencia JwtUserDetailsService.
     */
    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    /**
     * inyeccion de dependencia JwtTokenUtil.
     */
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    /**
     * Evalua solicitud entrante, valida si es una peticion autorizada.
     *
     * @param request  solicitud.
     * @param response respuesta.
     * @param chain    filter chain.
     * @throws ServletException exception checked.
     * @throws IOException      exception checked.
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                                    final FilterChain chain) throws ServletException, IOException {
        final String requestTokenHeader = request.getHeader("Authorization");
        String username = null;
        String jwtToken = null;
        // El token JWT tiene la forma de "Bearer token". Elimina la palabra Bearer y obtén solo el Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                logger.warn("No se ha podido obtener JWT token");
            } catch (ExpiredJwtException e) {
                logger.warn("El JWT Token ha expirado.");
            }
        } else {
            logger.warn("JWT Token no inicia con Bearer String");

        }
        //Una vez que obtengamos el token, validarlo.
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            try {
                UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);
                // si el token es válido, configure Spring Security para configurar manualmente la autenticación
                if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                            new UsernamePasswordAuthenticationToken(userDetails, null,
                                    userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken.setDetails(
                            new WebAuthenticationDetailsSource().buildDetails(request));
                    // Después de configurar la Autenticación en el contexto, especificamos
                    // que el usuario actual está autenticado.
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            } catch (Exception exception) {
                logger.warn(exception.getMessage());
            }
        }
        chain.doFilter(request, response);
    }

}
