package ni.com.jaslinfernando.config;

import ni.com.jaslinfernando.dto.FaultMsg;
import ni.com.jaslinfernando.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @since 1.0,
 */
@ControllerAdvice
public class ExceptionHandler implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Intercepta todas las excepciones de la clase Exception.
     * @param ex Exception generada.
     * @return ErrorMessage Response generico para errores.
     */
    @ResponseBody
    @org.springframework.web.bind.annotation.ExceptionHandler(java.lang.Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public FaultMsg handleBusinessException(final java.lang.Exception ex) {
        return new FaultMsg(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                ex.getMessage(), Objects.isNull(ex.getCause()) ? "" : ex.getCause().getMessage());
    }

    /**
     * Intercepta todas las excepciones de la clase NisumException.
     * @param ex Exception generada.
     * @return ErrorMessage Response generico para errores.
     */
    @ResponseBody
    @org.springframework.web.bind.annotation.ExceptionHandler(CustomException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public FaultMsg handleBusinessException(final CustomException ex) {
        return new FaultMsg(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                ex.getMessage(), Objects.isNull(ex.getCause()) ? "" : ex.getCause().getMessage());
    }

    /**
     * Intercepta todas las excepciones de la clase MethodArgumentNotValidException.
     * @param ex Exception generada.
     * @return ErrorMessage Response generico para errores.
     */
    @ResponseBody
    @org.springframework.web.bind.annotation.ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public FaultMsg handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex) {
        return new FaultMsg(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "Ha ocurrido un error.", ex.getBindingResult().getAllErrors().stream()
                .map(e -> e.getDefaultMessage()).collect(Collectors.joining(", ")));
    }

    /**
     * Intercepta todas las excepciones de la clase UsernameNotFoundException.
     * @param ex Exception generada.
     * @return ErrorMessage Response generico para errores.
     */
    @ResponseBody
    @org.springframework.web.bind.annotation.ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public FaultMsg handleUsernameNotFoundException(final UsernameNotFoundException ex) {
        return new FaultMsg(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(),
                Objects.isNull(ex.getCause()) ? "" : ex.getCause().getMessage());
    }
}
