package ni.com.jaslinfernando.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

/**
 * @author
 * @version 1.0
 * @see Configuration
 * @see Bean
 * @see ApiInfo
 * @see Docket
 * @since 1.0,
 */
@Configuration
public class SpringFoxConfig {

    /**
     * Crea Bean para Swagger.
     * @return Docket objeto de configuracion swagger.
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(s -> !s.contains("/error"))
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false);
    }

    /**
     * Crea objecto de información de contacto.
     * @return ApiInfo objecto de informacion de contacto.
     */
    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Api de Usuario",
                "Rest API usuario",
                "1.0.O",
                "",
                new Contact("Jaslin Flores", "https://gitlab.com/jaslinfernando", "jaslinfernando@gmail.com"),
                "MIT License",
                "https://opensource.org/licenses/mit-license.php",
                Collections.emptyList());
    }

}
