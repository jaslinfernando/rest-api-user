package ni.com.jaslinfernando.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ni.com.jaslinfernando.model.entity.EPhone;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see Getter
 * @since 1.0,
 */
@Getter
@Setter
public class PhoneDto implements Serializable {

    private static final long serialVersionUID = 1L;

    public PhoneDto() {
    }


    public PhoneDto(final EPhone EPhone) {
        this.id = EPhone.getId();
        this.number = EPhone.getNumber();
        this.cityCode = EPhone.getCityCode();
        this.countryCode = EPhone.getCountryCode();
    }


    public PhoneDto(final Integer id, final String number, final String cityCode, final String countryCode) {
        this.id = id;
        this.number = number;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
    }

    private Integer id;

    @NotEmpty(message = "Teléfono Requerido.")
    @JsonProperty("number")
    private String number;


    @NotEmpty(message = "Código Ciudad Requerido.")
    @JsonProperty("citycode")
    private String cityCode;

    @NotEmpty(message = "Código Pais Requerido.")
    @JsonProperty("contrycode")
    private String countryCode;

}
