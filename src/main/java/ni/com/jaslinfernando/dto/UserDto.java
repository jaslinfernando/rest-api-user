package ni.com.jaslinfernando.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ni.com.jaslinfernando.model.entity.EPhone;
import ni.com.jaslinfernando.model.entity.EUser;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Jaslin Flores, 24/02/2023
 * @version 1.0
 * @see Getter
 * @since 1.0,
 */
@Getter
@Setter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto implements Serializable {

    private static final long serialVersionUID = 1L;

    public UserDto() {

    }

    public UserDto(final EUser EUser) {
        this.id = EUser.getId();
        this.name = EUser.getName();
        this.email = EUser.getEmail();
        this.password = EUser.getPassword();
        this.token = EUser.getToken();
        this.active = EUser.getActive();
        this.phones = EUser.getPhones().stream().map(p -> new PhoneDto(p)).collect(Collectors.toSet());
        this.created = EUser.getCreated();
        this.modified = EUser.getModified();
        this.lastLogin = EUser.getLastLogin();
    }


    public UserDto(final UUID id, final String name, final String email, final String password, final String token,
                   final Boolean active, final Set<PhoneDto> phones) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.token = token;
        this.active = active;
        this.phones = phones;
    }
    @JsonProperty("id")
    private UUID id;
    @NotEmpty(message = "Nombre Requerido.")
    @JsonProperty("name")
    private String name;


    @NotNull(message = "Correo  Requerido.")
    @Email(regexp = "^[^@]+@[^@]+\\.[a-zA-Z]{2,}$", message = "Formato de correo invalido.")
    @JsonProperty("email")
    private String email;
    @NotEmpty(message = "Clave Requerida.")
    @Column(name = "password")
    @JsonProperty("password")
    private String password;
    @JsonProperty("token")
    private String token;
    @JsonProperty("active")
    private Boolean active;

    @NotNull(message = "Telefono Requerido.")
    @Size(min = 1, message = "Al menos un telefono.")
    @JsonProperty("phones")
    private @Valid Set<PhoneDto> phones;
    @JsonProperty("last_login")
    private LocalDateTime lastLogin;
    @JsonProperty("created")
    private LocalDateTime created;

    @JsonProperty("modified")
    private LocalDateTime modified;


    public EUser toEUser() {
        return new EUser(this.id, this.name, this.email, this.password, this.token, this.active,
                phones.stream().map(p -> new EPhone(p.getId(), null, p.getNumber(), p.getCityCode(), p.getCountryCode()))
                        .collect(Collectors.toSet())
                , null, LocalDateTime.now(), null);
    }

    public UserDto customResponseDto() {
        this.email = null;
        this.phones = null;
        this.name = null;
        this.password = null;
        return this;
    }



}
