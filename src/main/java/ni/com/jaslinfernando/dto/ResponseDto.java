package ni.com.jaslinfernando.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see AllArgsConstructor
 * @since 1.0, <i> Retorna la estructura del JSON generico para cada peticion.</i>
 * @param <T> Generico de la clase.
 */
@Getter
@Setter
@AllArgsConstructor
public class ResponseDto<T> {

    private T data;

    private Integer status;

    private String message;

    public ResponseDto() {

    }

    public ResponseDto(final T data) {
        this.data = data;
    }

}
