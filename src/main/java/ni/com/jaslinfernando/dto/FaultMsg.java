package ni.com.jaslinfernando.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @see Getter
 * @see Setter
 * @see AllArgsConstructor
 * @since 1.0,
 */
@Getter
@Setter
@AllArgsConstructor
public class FaultMsg {

    private Integer status;

    private String message;

    private String detail;

}
