package ni.com.jaslinfernando.controller;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import ni.com.jaslinfernando.dto.ResponseDto;
import ni.com.jaslinfernando.dto.FaultMsg;
import ni.com.jaslinfernando.dto.UserDto;
import ni.com.jaslinfernando.exception.CustomException;
import ni.com.jaslinfernando.repository.PhoneRepository;
import ni.com.jaslinfernando.service.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Jaslin Flores, 24/02/2023
 * @version 1.0
 * @see 
 * @since 1.0,
 */
@RestController
@RequestMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final IUser userSvc;

    @Autowired
    public UserController(final IUser userSvc) {
        this.userSvc = userSvc;
    }
    @Autowired
    PhoneRepository phoneRepository;


    @ApiOperation(value = "Lista de usuarios", produces = "application/json")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Listado exitoso.", response = ResponseDto.class),
            @io.swagger.annotations.ApiResponse(code = 500, message = "Error al obtener la lista", response = ResponseDto.class),
    })
    @GetMapping("/")
    public ResponseEntity<ResponseDto<List<UserDto>>> getAllUsers() {
        ResponseDto<List<UserDto>> response = new ResponseDto<>(new ArrayList<>());
        //usamos el map stream para transformar datos: se transforma de objetos EUser de la lista a UserDto
        //collect es un tipo especial de operacion terminal llamada Reductions donde todo el contenido del stream
        // es combinado en un primitivo simpe o un objeto en este caso de tipo List<UserDto>
        response.setData(userSvc.findAll().stream().map(x -> new UserDto(x)).collect(Collectors.toList()));
        response.setMessage("Listado satisfactorio.");
        response.setStatus(HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/buscar/{name}")
    @ApiOperation(value = "Buscar usuario por nombre", notes = "Sirve para listar un usuario con el nombre como parametro.")
    public  ResponseEntity<ResponseDto<UserDto>> getUsuarioByName(@ApiParam(value = "Nombre del usuario", required = true) @PathVariable("name") String name) throws CustomException {
        ResponseDto<UserDto> response = new ResponseDto<>();
        UserDto entity = new UserDto(userSvc.findUserByName(name));
        response.setData(entity);
        response.setMessage("Busqueda satisfactoria.");
        response.setStatus(HttpStatus.CREATED.value());
        return new ResponseEntity<>(response, HttpStatus.CREATED);


    }

    @GetMapping("/buscarById/{id}")
    @ApiOperation(value = "Buscar usuario por UUID", notes = "Sirve para listar un usuario con el id como parametro.")
    public  ResponseEntity<ResponseDto<UserDto>> getUsuarioById(@ApiParam(value = "Identificador del usuario", required = true) @PathVariable("id") final UUID id) throws CustomException {
        ResponseDto<UserDto> response = new ResponseDto<>();
        UserDto entity = new UserDto(userSvc.findEUserById(id));
        response.setData(entity);
        response.setMessage("Busqueda satisfactoria.");
        response.setStatus(HttpStatus.CREATED.value());
        return new ResponseEntity<>(response, HttpStatus.CREATED);


    }


    @ApiOperation(value = "Registra un usuario en especifico a traves de un JSON", notes = "Sirve para guardar un usuario con un JSON como parametro.", produces = "application/json")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 201, message = "éxito.",response = ResponseDto.class),
            @io.swagger.annotations.ApiResponse(code = 500, message = "Mensaje de error.", response = FaultMsg.class),
    })
    @PostMapping("/guardar")
    public ResponseEntity<ResponseDto<UserDto>> save(@Valid @RequestBody final UserDto user) throws CustomException {
        ResponseDto<UserDto> response = new ResponseDto<>();
        UserDto entity = new UserDto(userSvc.saveUser(user)).customResponseDto();
        response.setData(entity);
        response.setMessage("éxito.");
        response.setStatus(HttpStatus.CREATED.value());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}
