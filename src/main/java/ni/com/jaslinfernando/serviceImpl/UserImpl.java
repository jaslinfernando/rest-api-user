package ni.com.jaslinfernando.serviceImpl;

import ni.com.jaslinfernando.dto.UserDto;
import ni.com.jaslinfernando.exception.CustomException;
import ni.com.jaslinfernando.model.entity.EUser;
import ni.com.jaslinfernando.repository.UserRepository;
import ni.com.jaslinfernando.service.IUser;
import ni.com.jaslinfernando.config.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Jaslin Flores, 23/02/2023
 * @version 1.0
 * @since 1.0,
 * @see
 * @see
 * @see
 */
@Service
public class UserImpl implements IUser {

    /**
     *
     */
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;


    private final JwtTokenUtil jwtTokenUtil;


    @Value("${password.regularexpresion}")
    private String paswordRegexp;


    @Autowired
    public UserImpl(final UserRepository userRepository, final JwtTokenUtil jwtTokenUtil) {
        this.userRepository = userRepository;
        this.passwordEncoder = new BCryptPasswordEncoder();
        this.jwtTokenUtil = jwtTokenUtil;
    }


    @Override
    public List<EUser> findAll() {
        return userRepository.findAll();
    }

    @Override
    public EUser findUserByName(String name) {
        return userRepository.findUserByName(name);
    }

    @Override
    public EUser findEUserById(UUID id) throws CustomException {
        return userRepository.findEUserById(id);
    }


    @Override
    public EUser saveUser(final UserDto user) throws CustomException {
        validPassword(user.getPassword());
        validEmail(user.getEmail());
        EUser entity = user.toEUser();
        entity.setPassword(passwordEncoder.encode(user.getPassword()));
        entity.setCreated(LocalDateTime.now());
        entity.setToken(jwtTokenUtil.generateToken(user.getEmail()));
        entity.setLastLogin(entity.getCreated());
        entity.setActive(true);
        entity.getPhones().forEach(p -> {
            p.setUser(entity);
            p.setCreated(LocalDateTime.now());
        });
        return userRepository.save(entity);
    }

    private void validPassword(final String password) throws CustomException {
        Pattern pattern = Pattern.compile(paswordRegexp);
        Matcher matcher = pattern.matcher(password);
        if (!matcher.matches()) {
            throw new CustomException("Clave no cumple requerimientos.");
        }
    }

    private void validEmail(final String email) throws CustomException {
        Optional<EUser> user = userRepository.findUserEntityByEmail(email);
        if (user.isPresent()) {
            throw new CustomException("El correo ya registrado.");
        }
    }

}
