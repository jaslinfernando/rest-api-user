# Micro Servicio Usuario

## Stack

* [Publicar aplicación](http://localhost:8080/)
* [Autenticar](http://localhost:8080/authenticar/)
* [Agrega Usuario POST](http://localhost:8080/api/users/guardar/)
* [Listar usuarios GET](http://localhost:8080/api/usuarios/)
* [Swagger UI](http://localhost:8080/swagger-ui/)


## Ejecutar app

```bash
mvn spring-boot:run

```

## Autenticar
![agregar1](img/autentica1.png)

Se envia el usuario y la clave al endpoint ´/authenticar/´ para que se genere el token de autenticación que dura unos 30 minutos(esto es configurable)

username: **lieserl**
password: **Fernanda21@**

Ejemplo de token generado:
eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsaWVzZXJsIiwiZXhwIjoxNjc3NDI1MDA4LCJpYXQiOjE2Nzc0MjMyMDh9.0gpKd9Sm80n6z3vlD03UOYzPENzd3bN_M6yh3lQMVHNVbN2tF7Ozo7kXMZJHP1UV0eHq0wHRTCFP3_JlEs0L1g

## Agrega Usuario POST
![agregar1](img/agregar1.png)
Para agregar un Usuario nuevo se tiene que envir un JSON en el body de la petición POST como sigue:



```bash
{
    "name": "Jaslin Flores",
    "email": "jaslinfernando@gmail.com",
    "password": "GeliMaria19@",
    "phones": [
        {
            "number": "82621479",
            "citycode": "MGA",
            "contrycode": "NCA"
        }
    ]
}

```

O bien desde la linea de comandos ejecutamos un curl :

```bash

curl --location 'http://localhost:8080/api/users/guardar/' \
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsaWVzZXJsIiwiZXhwIjoxNjc3NDI1MDA4LCJpYXQiOjE2Nzc0MjMyMDh9.0gpKd9Sm80n6z3vlD03UOYzPENzd3bN_M6yh3lQMVHNVbN2tF7Ozo7kXMZJHP1UV0eHq0wHRTCFP3_JlEs0L1g' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Jaslin Flores",
    "email": "jaslinfernando@gmail.com",
    "password": "GeliMaria19@",
    "phones": [
        {
            "number": "82621479",
            "citycode": "MGA",
            "contrycode": "NCA"
        }
    ]
}'

	
```

## Listar usuarios GET
![listar1](img/listar2.png)
Asi se listan todos los usuarios: http://localhost:8080/api/users/

```bash

curl --location 'http://localhost:8080/api/users/' \
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsaWVzZXJsIiwiZXhwIjoxNjc3NDI1MDA4LCJpYXQiOjE2Nzc0MjMyMDh9.0gpKd9Sm80n6z3vlD03UOYzPENzd3bN_M6yh3lQMVHNVbN2tF7Ozo7kXMZJHP1UV0eHq0wHRTCFP3_JlEs0L1g'

```


## Swagger UI
La interfaz gráfica para la documentacion con swagger: http://localhost:8080/swagger-ui/
![swagger1](img/swagger1.png)


## Diagramas
 Autenticacón
![DiagramaAutenticar](img/DiagramaAutenticar.png)

 Usuario
![DiagramaUsuario](img/DiagramaUsuario.png)
